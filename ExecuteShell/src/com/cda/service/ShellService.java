package com.cda.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.cda.util.Constante;

public class ShellService {

	final static Logger logger = LoggerFactory.getLogger(ShellService.class);

	public String executeShellFOFListCorporativo(String codEjecucion) {

		String nameProcessShell = "[executeShellFOFListCorporativo]";
		String response = null;
		long tiempoInicio = System.currentTimeMillis();

		logger.info("Inicia ejecucion de proceso Shell: " + nameProcessShell);

		try {

			if (codEjecucion.equalsIgnoreCase(Constante.PARAMETRO.COD_EJECUCION)) {
				
				Process proc = Runtime.getRuntime().exec("/de/FIRCO/online/pe/web/FOF_LIST/FOFLIST_CORPORATIVO.sh /");
				
				logger.info("Imprime Process: " + proc.toString());
				
				BufferedReader read = new BufferedReader(new InputStreamReader(proc.getInputStream()));
				
				while (read.ready()) {
					logger.info("Imprimiendo lectura de Shell..." + read.readLine());
				}
				
				try {

					proc.waitFor();
					response = Constante.PARAMETRO.COD_RESPUESTA_EXITO;
					logger.info("*** Termina ejecucion de proceso Shell: " + nameProcessShell + " CON exito.");

				} catch (InterruptedException e) {

					logger.error("Error [InterruptedException] en el proceso Shell - " + nameProcessShell + ": " + e.getLocalizedMessage());
					response = Constante.PARAMETRO.COD_RESPUESTA_NO_EXITO;
				}
				

			} else {
				response = Constante.PARAMETRO.COD_RESPUESTA_NO_EXITO;
				logger.info("*** Termina ejecucion de proceso Shell: " + nameProcessShell + " SIN exito.");
			}

		} catch (IOException e) {
			logger.error("Error [IOException] en el proceso Shell - " + nameProcessShell + ": " + e.getMessage());
		} finally {
			long tiempoFin = System.currentTimeMillis();
			logger.info("------- Fin de proceso Shell: " + nameProcessShell + "------- ");
			logger.info(" Tiempo total de proceso Shell(ms):" + (tiempoFin - tiempoInicio) + "milisegundos");
		}

		return response;

	}

}
