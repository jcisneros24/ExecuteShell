package com.cda.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cda.service.ShellService;
import com.cda.util.Constante;


@WebServlet("/Servlet")
public class Servlet extends HttpServlet {
	
	final static Logger logger = LoggerFactory.getLogger(Servlet.class);

	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Servlet() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String codEjecucion = request.getParameter("name");
		String codRpta = null;
		String msjRpta = null;
		
		try {
			
			ShellService service = new ShellService();
			
			codRpta = service.executeShellFOFListCorporativo(codEjecucion);
			
			if (codRpta.equals(Constante.PARAMETRO.COD_RESPUESTA_EXITO)) {
				
				logger.info(Constante.PARAMETRO.MSJ_RESPUESTA_EXITO);
				msjRpta = Constante.PARAMETRO.MSJ_RESPUESTA_EXITO;
			} else {
				
				logger.info(Constante.PARAMETRO.MSJ_RESPUESTA_NO_EXITO);
				msjRpta = Constante.PARAMETRO.MSJ_RESPUESTA_NO_EXITO;
			}
			
			request.setAttribute("name", msjRpta);
			RequestDispatcher rd = request.getRequestDispatcher("respuesta.jsp");
			rd.forward(request, response);
			
		} catch (Exception e) {

			logger.info("Error IOException: : " + e.getLocalizedMessage());

		}

	}

}
