package com.cda.util;

public abstract class Constante {

	public abstract class PARAMETRO {
		
		public static final String COD_EJECUCION = "1";
		public static final String COD_RESPUESTA_EXITO = "1";
		public static final String COD_RESPUESTA_NO_EXITO = "0";
		public static final String MSJ_RESPUESTA_EXITO = "Se ejecuto correctamente el proceso SHELL.";
		public static final String MSJ_RESPUESTA_NO_EXITO = "No Se ejecuto correctamente el proceso SHELL.";

	}

}
